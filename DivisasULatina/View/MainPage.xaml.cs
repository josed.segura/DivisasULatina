﻿using DivisasULatina.viewmodel;
using Xamarin.Forms;

namespace DivisasULatina.View
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            InitializeComponent();
            BindingContext = new MainViewModel();
        }
    }
}
