﻿using System;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;
using Newtonsoft.Json;
using DivisasULatina.Model;
using Xamarin.Forms;

namespace DivisasULatina.viewmodel
{
    public class MainViewModel : INotifyPropertyChanged
    {

        #region Atributos

        private decimal amount;
        private double sourceRate;
        private double targetRate;
        private bool isEnabled;
        private bool isRunning;
        private string message;
        private ExchangeRates exchangeRates;
        private string _Status;
        public ICommand ConvierteCommand { get; set; }



        #endregion

        #region Prpiedades

        public ObservableCollection<Rate> Rates { get; set; }

        public string Status
        {
            set
            {
                if (_Status != value)
                {
                    _Status = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Status"));
                }
            }
            get
            {
                return _Status;
            }
        }

        public decimal Amount
        {
            set
            {
                if (amount != value)
                {
                    amount = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Amount"));
                }
            }
            get
            {
                return amount;
            }
        }

        public double SourceRate
        {
            set
            {
                if (sourceRate != value)
                {
                    sourceRate = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("SourceRate"));
                }
            }
            get
            {
                return sourceRate;
            }
        }

        public double TargetRate
        {
            set
            {
                if (targetRate != value)
                {
                    targetRate = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("TargetRate"));
                }
            }
            get
            {
                return targetRate;
            }
        }

        public bool IsEnable
        {
            set
            {
                if (isEnabled != value)
                {
                    isEnabled = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsEnabled"));
                }
            }
            get
            {
                return isEnabled;
            }
        }


        public bool IsRunning
        {
            set
            {
                if (isRunning != value)
                {
                    isRunning = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("IsRunning"));
                }
            }
            get
            {
                return isRunning;
            }
        }

        public string Message
        {
            set
            {
                if (message != value)
                {
                    message = value;
                    PropertyChanged?.Invoke(this, new PropertyChangedEventArgs("Message"));
                }
            }
            get
            {
                return message;
            }
        }


        #endregion




        #region Contructores

        public MainViewModel()
        {

            InitCommands();
            Message = "Enter an amount, select a source currency, select a target currency and press 'Convert' button";

            Rates = new ObservableCollection<Rate>();
            GetRates();


        }



        #endregion

        #region Metodos


        private async Task GetRates()
        {
            IsRunning = true;



            try
            {
                var client = new HttpClient();
                client.BaseAddress = new Uri("https://openexchangerates.org");
                var url = "/api/latest.json?app_id=37553707154b418ba799b61df8fba185";
                var response = await client.GetAsync(url);
                if (!response.IsSuccessStatusCode)
                {
                    await Application.Current.MainPage.DisplayAlert("Hla","Hola","ok");
                    Message = response.StatusCode.ToString();
                    IsRunning = false;
                    return;
                }

                var result = await response.Content.ReadAsStringAsync();
                exchangeRates = JsonConvert.DeserializeObject<ExchangeRates>(result);

            }
            catch (Exception ex)
            {
                Message = ex.Message;
                IsRunning = false;
                return;
            }

            LoadRates();
            IsRunning = false;
            isEnabled = true;
        }

        private void LoadRates()
        {

            var type = typeof(Rates);
            var properties = type.GetRuntimeFields();

            foreach (var property in properties)
            {
                var code = property.Name.Substring(1, 3);
                Rates.Add(new Rate
                {
                    Code = code,
                    TaxRate = (double)property.GetValue(exchangeRates.Rates),
                });
            }
        }

        private void InitCommands()
        {
            ConvierteCommand = new Command(ConvertMoney);
        }

        private void ConvertMoney()
        {
            var converted = (Amount / (decimal)SourceRate) * (decimal)TargetRate;
            Message = string.Format("{0:C2} = {1:C2}", Amount, converted);

        }



        #endregion


        #region Eventos

        public event PropertyChangedEventHandler PropertyChanged;

        #endregion
    }
}
